import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Constants } from '../../shared/constants/Constants';
import { AppointmentType } from '../models/AppointmentType';
import { AppointmentTypeService } from '../services/appointment-type.service';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-appointment-type',
  templateUrl: './appointment-type.component.html'
})
export class AppointmentTypeComponent implements OnInit {
  formAppointmentType: FormGroup = new FormGroup({});
  isEditing!: boolean;
  appointmentTypeDialog!: boolean;
  busqueda!: string;
  active!: boolean;
  appointmentTypes!: AppointmentType[];
  displaySaveDialog!: boolean;
  progressSpinnerDlg = false;
  @ViewChild('dt')
  table!: Table;
  paginatorValue!: boolean;
  rowsPerPage!: number[];
  responsiveLayout!: string;
  templateValue!: string;
  constructor(
    private appointmentTypeService: AppointmentTypeService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    this.formAppointmentType = new FormGroup({
      id: new FormControl(''),
      name: new FormControl('', Validators.required),
      description: new FormControl(),
      color: new FormControl('#1976D2',Validators.required),
      durationMins: new FormControl (0,Validators.required),
      status: new FormControl(true),
      dateInsert: new FormControl(),
    });
  }

  async ngOnInit() {
    this.progressSpinnerDlg = true;
    this.paginatorValue = Constants.PAGINATOR_VALUE;
    this.rowsPerPage = Constants.ROWS_PER_PAGE;
    this.responsiveLayout = Constants.RESPONSIVE_LAYOUT_VALUE;
    this.templateValue = Constants.TEMPLATE;
    this.appointmentTypes =
      await (await this.appointmentTypeService.getAllAppointmentTypes()).result;
    this.progressSpinnerDlg = false;
  }

  hideDialog() {
    this.appointmentTypeDialog = false;
    this.isEditing = false;
    this.formAppointmentType.reset();
  }

  async submitAppointmentType() {
    this.progressSpinnerDlg = true;
    if (this.formAppointmentType.valid) {
     
      const appointmentType = this.formAppointmentType.value;
    
      await this.appointmentTypeService
        .saveAppointmentType(appointmentType)
        .then((value) => {
         
          if(value.status===200){
            const appointmentTypeSave = value.result;
            this.updateAppointmentTypeList(appointmentTypeSave);
          
          this.messageService.add({
            severity: 'success',
            life: Constants.LIFE_TIME_ALERTS,
            summary: 'Exitoso',
            detail: value.message
          });
        }else{
          this.messageService.add({
            severity: 'error',
            life: Constants.LIFE_TIME_ALERTS,
            summary: 'Error',
            detail: value.message
          });

        }
        });
      this.appointmentTypeDialog = false;
      this.formAppointmentType.reset();
    } else {
      this.formAppointmentType.markAllAsTouched();
      this.messageService.add({
        severity: 'error',
        life: Constants.LIFE_TIME_ALERTS,
        summary: 'Por favor revise la información',
        detail: 'Errores en el formulario'
      });
    }
    this.progressSpinnerDlg = false;
  }

  updateAppointmentTypeList(appointmentType: AppointmentType) {
    const updated = this.appointmentTypes
      .filter((e) => e.id === appointmentType.id)
      .map((f) => f == appointmentType);

    if (updated.length === 0) this.appointmentTypes.push(appointmentType);
    else {
      this.findIndexById(appointmentType);
    }
  }

  createForm() {
    this.progressSpinnerDlg = true;
    this.formAppointmentType = new FormGroup({
      id: new FormControl(),

      name: new FormControl('', [
        Validators.required,
        Validators.minLength(Constants.MIN_LENGTH_INPUTS),
        Validators.maxLength(Constants.MAX_LENGTH_INPUTS)
      ]),
      color: new FormControl('#1976D2',Validators.required),
      durationMins: new FormControl (0,Validators.required),
      description: new FormControl('', [
        
        Validators.maxLength(Constants.MAX_LENGTH_INPUT_DESCRIPTION)
      ]),
      status: new FormControl(true)
    });

    this.appointmentTypeDialog = true;
    this.isEditing = false;
    this.progressSpinnerDlg = false;
  }

  editForm(appointmentType: AppointmentType) {
    this.progressSpinnerDlg = true;
    this.formAppointmentType = new FormGroup({
      name: new FormControl(appointmentType.name, [
        Validators.required,
        Validators.minLength(Constants.MIN_LENGTH_INPUTS),
        Validators.maxLength(Constants.MAX_LENGTH_INPUTS)
      ]),
      color: new FormControl(appointmentType.color,  Validators.required),
      durationMins: new FormControl (appointmentType.durationMins,Validators.required),
      description: new FormControl(appointmentType.description, [
        
        Validators.maxLength(Constants.MAX_LENGTH_INPUT_DESCRIPTION)
      ]),
     

      id: new FormControl(appointmentType.id),

      status: new FormControl(
        appointmentType.status
      ),
      dateInsert: new FormControl(
        appointmentType.dateInsert
      )
    });
    this.appointmentTypeDialog = true;
    this.isEditing = true;
    this.progressSpinnerDlg = false;
  }


  confirm(event: Event, appointmentType: AppointmentType) {
    this.confirmationService.confirm({
      target: event.target as EventTarget,
        message: '¿Está seguro de eliminar el item seleccionado?',
        icon: 'pi pi-exclamation-triangle',
          accept: () => {
            this.deleteAppointmenyType(appointmentType);
            
        },
        reject: () => {
            
        }
    });
}

async deleteAppointmenyType(appointmentType: AppointmentType){
  const result = await this.appointmentTypeService.deleteAppointmentType(appointmentType);

  if (result.status===200){
    this.deleteIndexById(appointmentType);
    this.messageService.add({
      severity: 'success',
      life: Constants.LIFE_TIME_ALERTS,
      summary: result.message,
      detail: 'Eliminado correctamente'
    });
  }else{
    this.messageService.add({
      severity: 'error',
      life: Constants.LIFE_TIME_ALERTS,
      summary: result.message,
      detail: 'Error'
    });
  }

}
  findIndexById(appointmentType: AppointmentType) {
    let index = -1;
    for (let i = 0; i < this.appointmentTypes.length; i++) {
      if (this.appointmentTypes[i].id === appointmentType.id) {
        index = i;
        break;
      }
    }

    this.appointmentTypes[index] = appointmentType;
  }

  clear(table: Table) {
    this.busqueda = '';
    table.clear();
  }

  deleteIndexById(appointmentType: AppointmentType) {
    let index = -1;
    for (let i = 0; i < this.appointmentTypes.length; i++) {
      if (this.appointmentTypes[i].id === appointmentType.id) {
        index = i;
        break;
      }
    }

   
    this.appointmentTypes.splice(index,1);
  }
}
