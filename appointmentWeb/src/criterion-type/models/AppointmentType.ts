export class AppointmentType {
  public id!: number;
  public name!: string;
  public description!: string;
  public color!: string;
  public durationMins!: number;
  public status!: boolean;
  public dateInsert!: Date;
  public dateAlter!: Date;

}
