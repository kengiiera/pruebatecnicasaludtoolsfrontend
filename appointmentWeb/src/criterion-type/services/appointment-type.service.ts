import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { lastValueFrom } from 'rxjs';
import { Response } from '../../shared/model/Response';
import { constantsUrlApi } from '../../shared/constants/ConstantsUrlApi';
import { AppointmentType } from '../models/AppointmentType';
const options = {
  observe: 'body' as const,
  responseType: 'json' as const
};

@Injectable({
  providedIn: 'root'
})
export class AppointmentTypeService {
  constructor(private http: HttpClient) {}

  async getAllAppointmentTypes(): Promise<Response> {
    return await lastValueFrom(
      this.http.get<Response>(
        `${constantsUrlApi.APPOINTMENT_TYPE_API}`,
        options
      )
    );
  }



  async saveAppointmentType(
    appointmentType: AppointmentType
  ): Promise<Response> {
    return await lastValueFrom(
      this.http.post<Response>(
        `${constantsUrlApi.APPOINTMENT_TYPE_API}`,
        appointmentType,
        options
      )
    );
  }

  async deleteAppointmentType(
    appointmentType: AppointmentType
  ): Promise<Response> {
    return await lastValueFrom(
      this.http.delete<Response>(
        `${constantsUrlApi.APPOINTMENT_TYPE_API}/${appointmentType.id}`

      )
    );
  }
}
