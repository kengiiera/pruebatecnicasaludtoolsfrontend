import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppointmentTypeModule } from '../criterion-type/appointment-type.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppointmentTypeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
