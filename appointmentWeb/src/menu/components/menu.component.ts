import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  items!: MenuItem[];

  constructor(
   
  ) {}

  async ngOnInit() {

  }
  async logout() {
    const url = "https://www.saludtools.com/";
    localStorage.clear();
    window.location.assign(url);
  }
}
